import 'dart:async';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/services/authProblem.dart';

abstract class BaseAuth {
  Future<String> signIn(String email, String password);

  Future<String> signUp(String email, String password);

  Future<FirebaseUser> getCurrentUser();

  Future<void> sendEmailVerification();

  Future<void> signOut();

  Future<bool> isEmailVerified();

  Future<void> resetPassword(String email);

  Future<void> sendPasswordResetEmail(String email);

}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<String> signIn(String email, String password) async {

    try {
      AuthResult result = await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      return user.uid;
    } catch (e) {
      authProblems errorType;
      if (Platform.isAndroid) {
        switch (e.message) {
          case Constants.USER_NOT_FOUND:
            ActionUtils.showToast(Constants.USER_NOT_FOUND);
            return "error";
            break;
          case Constants.USER_PASSWORD_WRONG:
            ActionUtils.showToast(Constants.USER_PASSWORD_WRONG);
            return "error";
            break;
          case Constants.NETWORK_ERROR:
            ActionUtils.showToast(Constants.USER_PASSWORD_WRONG);
            return "error";
            break;
        // ...
          default:
            print('Case ${e.message} is not yet implemented');
            ActionUtils.showToast('Case ${e.message} is not yet implemented');


        }
      } else if (Platform.isIOS) {
        switch (e.code) {
          case 'Error 17011':
            errorType = authProblems.UserNotFound;
            break;
          case 'Error 17009':
            errorType = authProblems.PasswordNotValid;
            break;
          case 'Error 17020':
            errorType = authProblems.NetworkError;
            break;
        // ...
          default:
            print('Case ${e.message} is not yet implemented');
        }
      }
      print('The error is $errorType');
    }
  }
  Future<String> signUp(String email, String password) async {
    try {
      AuthResult result = await _firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      return user.uid;
    } catch (e) {
      ActionUtils.showToast((e.message));
      return "error";
    }
  }


  Future<void> sendPasswordResetEmail(String email) async {
    return _firebaseAuth.sendPasswordResetEmail(email: email);
  }

  Future<void> resetPassword(String email) async {
    try{
    return await _firebaseAuth.sendPasswordResetEmail(email: email);
    }catch(e){
      print("exaption");
    }
  }


  Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendEmailVerification() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    user.sendEmailVerification();
  }

  Future<bool> isEmailVerified() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user.isEmailVerified;
  }
}
