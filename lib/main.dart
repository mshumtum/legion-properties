import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/pages/StartScreen/root_page.dart';
import 'package:real_estate/services/authentication.dart';
import 'package:splashscreen/splashscreen.dart';

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new  MyApp(),
    theme: new ThemeData(
      primarySwatch: Colors.brown,
    ),
  ));
}


class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppSplash();
  }
}

class _MyAppSplash extends State<MyApp> {


  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 4,
        navigateAfterSeconds: new RootPage(auth: new Auth()),
        title: new Text(
          'Howell Properties',
          style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
        ),
        image: new Image.asset('images/icon.png'),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        onClick: () => print("Flutter Egypt"),
        loaderColor: Colors.brown);
  }

  @override
  void initState() {
    getAdminData();
  }

  void getAdminData() {

    ActionUtils.checkInternet().then((intenet) {
      if (intenet != null && intenet) {
         Firestore.instance
            .collection(Constants.ADMIN)
            .document(Constants.ADMIN_LIST).get().then((value) => saveOffline(value));
      }
      else{
        // No-Internet Case
        ActionUtils.showToast(Constants.INTERNET);
      }});
  }

  saveOffline(DocumentSnapshot value) {
    Constants.ADMIN_EMAILS=value.data["emails"];

  }
}
