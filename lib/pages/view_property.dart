import 'package:carousel_pro/carousel_pro.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/pages/Chat/chat.dart';
import 'package:real_estate/pages/edit_property.dart';
import 'package:real_estate/services/database.dart';
import 'package:share/share.dart';

import 'ImagePreview.dart';

class ViewProperty extends StatefulWidget {
  ViewProperty({this.userId,this.docId});

  final String userId;
  final String docId;

  @override
  _ViewPropertyState createState() => _ViewPropertyState();
}

class _ViewPropertyState extends State<ViewProperty> {
  List<DocumentSnapshot> _documentList;
  String ownerContact = "";
  String ownerName = "";
  String houseNo = "";
  String houseDescription = "";
  String streetName = "";
  String address = "";
  String landmark = "";
  String userId = "";
  String brokerName = "";
  String brokerChatName = "";
  List<dynamic> imgUrl;
  String lat = "";
  String lng = "";
  String pin = "";
  String propertyStatus = "";
  List<NetworkImage> propertyPic = List<NetworkImage>();
  DatabaseMethods databaseMethods = new DatabaseMethods();
  final databaseReference = Firestore.instance;
  @override
  void initState() {
    _documentList = new List();
    imgUrl = new List();
    imgUrl.add(Constants.PROPERTY_IMAGE);
    getData(widget.docId);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(Constants.PROPERTY_DETAILS),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              _onShareTap();
            },
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          _showForm(context),
        ],
      ),
    );
  }

  Widget _showForm(BuildContext context) {
    if (ownerName != "") {
      return new Container(
        padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 12.0),
        child: new ListView(
          shrinkWrap: true,
          children: <Widget>[
            showImage(context),
            showHouseName(),
            showStreetName(),
            showAddressPin(),
            new Divider(
              color: Colors.grey,
            ),
            showCallMapIcon(),
            showBuyChat(),
            new Divider(
              color: Colors.grey,
            ),
            showDiscription(),
          ],
        ),
      );
    } else {
      return Center(child: CircularProgressIndicator());
    }
  }

/*   onTap: (){
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ImagePreview(imgUrl[0])
            ));
      }*/
  Widget showImage(BuildContext context) {
    return new SizedBox(
        height: 220.0,
        width: 350.0,
        child: Carousel(
          images: propertyPic,
          dotSize: 4.0,
          dotSpacing: 15.0,
          dotColor: Colors.lightGreenAccent,
          indicatorBgPadding: 5.0,
          dotBgColor: Colors.brown.withOpacity(0.3),
          borderRadius: true,
          autoplay: false,
          onImageTap: (index) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ImagePreview(imgUrl[index])));
          },
        ));
  }

  Widget showOwnerInfo() {
    return new Container(
      child: new Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
              child: new FlatButton(
                child: new Text(ownerName,
                    style: new TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.black87)),
                onPressed: null,
              ),
            ),
          ]),
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget showHouseName() {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.fromLTRB(10.0, 15.0, 0.0, 0.0),
      child: new Text(houseNo,
          style: new TextStyle(
              fontSize: 19.0,
              fontWeight: FontWeight.w500,
              color: Colors.black87)),
    );
  }

  Widget showStreetName() {
    return Container(
      alignment: Alignment.centerLeft,
      child: new Text(streetName,
          style: new TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w400,
              color: Colors.black54)),
      padding: const EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 0.0),
    );
  }

  Widget showAddressPin() {
    return Container(
      alignment: Alignment.centerLeft,
      child: new Text(address + " - " + pin,
          style: new TextStyle(
              fontSize: 17.0,
              fontWeight: FontWeight.w300,
              color: Colors.black54)),
      padding: const EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 8.0),
    );
  }

  Widget showCallMapIcon() {
    return new Container(
      child: new Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
              child: new FlatButton(
                child: new Text(ownerName,
                    style: new TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w300,
                        color: Colors.black87)),
                onPressed: chatMethod,
              ),
            ),
            IconButton(
              icon: Icon(Icons.call),
              onPressed: () {
                ActionUtils.launchCaller(ownerContact);
              },
            ),
            IconButton(
              icon: Icon(Icons.gps_fixed),
              onPressed: () {
                ActionUtils.openMap(double.parse(lat), double.parse(lng));
              },
            )
          ]),
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget showBuyChat() {
    if (widget.userId != userId) {
      return new Container(
        child: new Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Container(
                padding: const EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
              ),
              new Container(
                padding: const EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                child: new RaisedButton(
                  child: new Text("Chat",
                      style: new TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300,
                          color: Colors.black87)),
                  onPressed: chatMethod,
                ),
              ),
            ]),
        padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        alignment: Alignment.centerLeft,
      );
    } else {

      return new Container(
        child: new Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Container(
                padding: const EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                child: new RaisedButton(
                  child: new Text("Edit",
                      style: new TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300,
                          color: Colors.black87)),
                  onPressed: () => {
                  Navigator.push(
                  context,
                  MaterialPageRoute(
                  builder: (context) => new EditProperty(docId: widget.docId)),
                  ).then((val) => {getData(widget.docId)})
                  },
                ),
              ),
              new Container(
                padding: const EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                child: new RaisedButton(
                  child: new Text("Delete",
                      style: new TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300,
                          color: Colors.black87)),
                  onPressed: deleteMethod,
                ),
              ),
            ]),
        padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        alignment: Alignment.centerLeft,
      );
    }
  }
  void deleteMethod() {
    try {
      databaseReference.collection(Constants.PROPERTIES).document(widget.docId).delete().then((value) => resetScreen());
    } catch (e) {
      print(e.toString());
    }
  }

  Widget showDiscription() {
    return Container(
      alignment: Alignment.centerLeft,
      child: new Text(
        "Property Status : " +propertyStatus+" \n"+
          "Description : \n" +
              houseDescription +
              " \n\n" +
              "Landmark : " +
              landmark,
          style: new TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w300,
              color: Colors.black)),
      padding: const EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 8.0),
    );
  }

  void getData(String docId) async {
    Firestore.instance
        .collection(Constants.PROPERTIES)
        .document(docId)
        .get()
        .then((value) => {
              setState(() {
                ownerContact = value.data["ownerContact"];
                ownerName = value.data["ownerName"];
                houseNo = value.data["houseNo"];
                houseDescription = value.data["houseDescription"];
                streetName = value.data["streetName"];
                address = value.data["address"];
                imgUrl.clear();
                imgUrl = value.data["imgUrl"];
                userId = value.data["userId"];
                lat = value.data["lat"];
                landmark = value.data["landmark"];
                lng = value.data["lng"];
                pin = value.data["pin"];
                propertyStatus = value.data["propertyStatus"];

                brokerChatName = value.data["brokerChatName"];
                propertyPic.clear();
                for (var i = 0; i < imgUrl.length; i++) {
                  propertyPic.add(NetworkImage(imgUrl[i]));
                  //print(imgUrl[i]);
                }
              })
            });
  }
  void _onShareTap() {
    final RenderBox box = context.findRenderObject();
    Share.share(Constants.SHARE_TEXT,
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  chatMethod() {
    if (widget.userId == userId) {
      ActionUtils.showToast("Current user and target user are same");
    } else {
      List<String> users = [brokerChatName, Constants.USER_CHAT_NAME];

      String chatRoomId =
          getChatRoomId(brokerChatName, Constants.USER_CHAT_NAME);

      Map<String, dynamic> chatRoom = {
        "users": users,
        "chatRoomId": chatRoomId,
      };

      databaseMethods.addChatRoom(chatRoom, chatRoomId);

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Chat(
                    chatRoomId: chatRoomId,
                  )));
    }
  }

  getChatRoomId(String a, String b) {
    if (a.substring(0, 1).codeUnitAt(0) > b.substring(0, 1).codeUnitAt(0)) {
      return "$b\_$a";
    } else {
      return "$a\_$b";
    }
  }

  resetScreen() {
    Navigator.pop(context);
    setState(() {
      Constants.SCREEN_RESET=1;
    });
  }
}
