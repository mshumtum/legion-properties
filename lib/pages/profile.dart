import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/pages/editProfile.dart';
import 'package:real_estate/pages/PropertyByStatus.dart';
import 'package:real_estate/pages/view_property.dart';
import 'package:real_estate/services/authentication.dart';
import 'package:url_launcher/url_launcher.dart';

class Profile extends StatefulWidget {
  Profile({ this.auth, this.logoutCallback});

  final BaseAuth auth;
  final VoidCallback logoutCallback;

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with SingleTickerProviderStateMixin {
  TabController _tabController;
  String _image = Constants.USER_IMAGE;
  String address = "",title;
  String gender = "";
  List<DocumentSnapshot> documentList;

  @override
  void initState() {
    super.initState();
    documentList=new List();
    getMyProperties();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            centerTitle: true,
            title: Text(
              'Profile',
              style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
            ),
            actions: <Widget>[
              IconButton(onPressed: () {
                openSignOutDialog();
              }, icon: Icon(Icons.exit_to_app)),
              SizedBox(
                width: 10.0,
              )
            ]),
        body: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.fromLTRB(
                    0.0, MediaQuery
                    .of(context)
                    .size
                    .height / 25, 0.0, 40.0),
                child: Column(children: <Widget>[
                  Row(
                    children: <Widget>[
                      SizedBox(width: 15.0),
                      profilePic(),
                      SizedBox(width: 5.0),
                      Expanded(
                        child: personalDetails(),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  addressInfo(),
                  SizedBox(height: 20.0),
                  editButton(),
                  SizedBox(height: 20.0),
                  optionButton(),
                ]))));
  }

  profilePic() {
    return Center(
      child: Column(
        children: <Widget>[
          CircleAvatar(
              backgroundColor: Colors.blueAccent,
              backgroundImage: NetworkImage(Constants.USER_PIC),
              radius: MediaQuery
                  .of(context)
                  .size
                  .width / 9),
          SizedBox(height: 8.0),
          Text(Constants.USER_NAME,
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.black)),
        ],
      ),
    );
  }

  personalDetails() {
    return Container(
        padding: EdgeInsets.fromLTRB(2.0, 10.0, 2.0, 10.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Text(Constants.USER_EMAIL,
                        style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.black87)),
                  )
                ],
              ),
              SizedBox(
                height: 8.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Constants.USER_GENDER == null ? Text("") : Text(
                        Constants.USER_GENDER,
                        style: TextStyle(
                            fontSize: 15.0,
                            color: Colors.black45,
                            fontWeight: FontWeight.w500)),
                  ),
                ],
              ),
              SizedBox(
                height: 8.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Text(Constants.USER_CONTACT,
                        style: TextStyle(
                            fontSize: 15.0,
                            color: Colors.black45,
                            fontWeight: FontWeight.w600)),
                  )
                ],
              ),
              SizedBox(
                height: 15.0,
              ),
            ]));
  }

  editButton() {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 15.0,
        ),
        Expanded(
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
              side: BorderSide(color: Colors.grey),
            ),
            onPressed: () {
              print('edit');
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => EditProfile()));
            },
            color: Colors.white,
            elevation: 0.0,
            child: Text('Edit Profile',
                style: TextStyle(
                    fontSize: 17.0,
                    color: Colors.black54,
                    fontWeight: FontWeight.w500)),
          ),
        ),
        SizedBox(
          width: 15.0,
        ),
      ],
    );
  }

  addressInfo() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            SizedBox(
              width: 15.0,
            ),
            Text(Constants.USER_LOCALITY,
                style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.w300,
                    color: Colors.black)),
            SizedBox(
              width: 15.0,
            ),
          ],
        ),
        Divider(
          thickness: 1.0,
          endIndent: 20.0,
          indent: 20.0,
        ),
        GestureDetector(
          child: Text(Constants.USER_WEB,
              style: TextStyle(
                  fontSize: 15.0, decoration: TextDecoration.underline,
                  fontWeight: FontWeight.w600,
                  color: Colors.blueAccent)),
          onTap: () {
            print(Constants.USER_WEB);
            launch("https://${Constants.USER_WEB}");
          },
        ),
      ],
    );
  }

  optionButton() {
    return Container(
        height: MediaQuery
            .of(context)
            .size
            .height,
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
              child: TabBar(
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.tab,
                tabs: <Widget>[
                  Tab(
                    icon: Icon(
                      Icons.view_comfy,
                      color: Colors.black,
                      size: 25.0,
                    ),
                  ),
                  Tab(
                    icon: Icon(
                      Icons.format_list_bulleted,
                      color: Colors.black,
                      size: 25.0,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20.0),
            Expanded(
              child: Container(
                child:
                TabBarView(controller: _tabController, children: <Widget>[
                  propertyList(),
                  propertyDetails(),
                ]),
              ),
            )
          ],
        ));
  }

  propertyDetails() {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  children: <Widget>[
                    InkWell(
                        onTap: () {
                          print('Active Property');
                          setState(() {
                            title  = Constants.ACTIVE_PROPERTY;
                          });
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>Details(title)));
                        },
                        child: SizedBox(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height / 6,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width / 3,
                          child: Card(
                            color: Colors.white,elevation: 3.0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            child: ClipRRect(
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              borderRadius: BorderRadius.circular(20.0),
                              child: Image.network(
                                Constants.ACTIVE_PROP_IMAGE,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        )),
                    Text( Constants.ACTIVE_PROPERTY,
                        style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.black54,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    InkWell(
                        onTap: () {
                          print('sold property');
                          setState(() {
                            title  =  Constants.SOLD_PROPERTY;
                          });
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>Details(title)));
                        },
                        child: SizedBox(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height / 6,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width / 3,
                          child: Card(
                              color: Colors.white,elevation: 3.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              child: ClipRRect(
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                borderRadius: BorderRadius.circular(20.0),
                                child: Image.network(
                                  Constants.SOLD_IMAGE,
                                  fit: BoxFit.cover,
                                ),
                              )),
                        )),
                    Text(Constants.SOLD_PROPERTY,
                        style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.black54,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 15.0),
          Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  children: <Widget>[
                    InkWell(
                        onTap: () {
                          print(Constants.UNDER_CONSTRUCTION);
                          setState(() {
                            title  = Constants.UNDER_CONSTRUCTION;
                          });
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>Details(title)));
                        },
                        child: SizedBox(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height / 6,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width / 3,
                          child: Card(
                              color: Colors.white,elevation: 3.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              child: ClipRRect(
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                borderRadius: BorderRadius.circular(20.0),
                                child: Image.network(
                                  Constants.UNDER_CONSTRUCTION_IMAGE,
                                  fit: BoxFit.cover,
                                ),
                              )),
                        )),
                    Text(Constants.UNDER_CONSTRUCTION,
                        style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.black54,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  propertyList() {
    if (documentList.length > 0) {
      return Container(
          padding: EdgeInsets.all(10.0),
          child: GridView.builder(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 200,
                  crossAxisSpacing: 5.0,
                  mainAxisSpacing: 5.0),
              itemCount: documentList.length,
              itemBuilder: (context, index) {
                String docId = documentList[index].data["docId"];
                List<dynamic> imgArray = documentList[index].data["imgUrl"];
                String imgUrl=imgArray[0];

                return Container(
                  child: InkWell(
                      onTap: () {
                        _launchView(docId);
                      },
                      child: SizedBox(
                        height: MediaQuery
                            .of(context)
                            .size
                            .height / 6,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width / 3,
                        child: Card(
                          color: Colors.blueAccent,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                          child: ClipRRect(
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            borderRadius: BorderRadius.circular(20.0),
                            child: Image.network(
                              imgUrl,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      )),
                );
              }));
    }else{
      return Container();
    }
  }

  openSignOutDialog() async {
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new Container(
              child: Text("Are You Sure ?"),
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              new FlatButton(
                  child: const Text('Logout'),
                  onPressed: () {
                    Navigator.pop(context);
                    signOut();
                  })
            ],
          );
        });
  }
  void _launchView(String i) {
    //  print( documentList[i].data);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => new ViewProperty(userId: Constants.USER_ID,docId: i)),
    ).then((value) => getMyProperties);
  }

  getMyProperties() {
    Firestore.instance
        .collection(Constants.PROPERTIES)
        .where("userId", isEqualTo: Constants.USER_ID)
        .getDocuments()
        .then((value) =>
        setState(() {
          documentList = value.documents;
        }));
  }

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }
}
