import 'package:carousel_pro/carousel_pro.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/pages/comments.dart';
import 'package:real_estate/pages/edit_property.dart';
import 'package:real_estate/pages/view_property.dart';
import 'package:share/share.dart';

class Details extends StatefulWidget {
  String title;
  Details(this.title);
  @override
  _DetailsState createState() => _DetailsState(title);
}

class _DetailsState extends State<Details> {
  String title;
  _DetailsState(this.title);

  List documentList;
  List<bool> likeMainArray;

  @override
  void initState() {
    super.initState();
    documentList=new List();
    getMyPropertiesDetails();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          title,
          style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w500,
              color: Colors.white),
        ),
      ),
      body: _showTodoList(),
    );
  }

  Widget textOutput(String ownerInfo) {
    return Text(ownerInfo,
        style: TextStyle(
            fontSize: 13.0,
            fontWeight: FontWeight.w500,
            color: Colors.black54));
  }

  Widget _showTodoList() {
    if (documentList.length > 0) {
      return ListView.builder(
          shrinkWrap: true,
          itemCount: documentList.length,
          itemBuilder: (BuildContext context, int index) {
            String ownerContact = documentList[index].data["ownerContact"];
            String ownerName = documentList[index].data["ownerName"];
            String houseNo = documentList[index].data["houseNo"];
            String profileImg = documentList[index].data["profileImg"];
            List<dynamic> imgUrl = documentList[index].data["imgUrl"];
            String docId = documentList[index].data["docId"];
            String userId = documentList[index].data["userId"];
            String lat = documentList[index].data["lat"];
            String lng = documentList[index].data["listlng"];
            List<NetworkImage> propertyPic = List<NetworkImage>();
            for (var i = 0; i < imgUrl.length; i++) {
              propertyPic.add(NetworkImage(imgUrl[i]));
              //print(imgUrl[i]);
            }


            return Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 10.0),
                    Card(
                      color: Colors.white,
                      //  clipBehavior: Clip.antiAliasWithSaveLayer,
                      //semanticContainer: true,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0)),
                      elevation: 8.0,
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              SizedBox(
                                width: 5.0,
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(5, 3, 5, 1),
                                child: CircleAvatar(
                                  backgroundImage: NetworkImage(profileImg),
                                  radius: 20.0,
                                ),
                              ),
                              Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[textOutput(ownerName)],
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Row(
                                        children: <Widget>[textOutput(houseNo)],
                                      )
                                    ],
                                  )),
                            ],
                          ),
                          Divider(
                            color: Colors.brown,
                          ),
                          SizedBox(
                              height: 220.0,
                              width: 350.0,
                              child: Carousel(
                                images: propertyPic,
                                dotSize: 4.0,
                                dotSpacing: 15.0,
                                dotColor: Colors.lightGreenAccent,
                                indicatorBgPadding: 5.0,
                                dotBgColor: Colors.brown.withOpacity(0.3),
                                borderRadius: true,
                                autoplay: false,
                                onImageTap: (index) {
                                  _launchView(docId);
                                },
                              )),
                          Row(
                            children: <Widget>[
                              SizedBox(width: 15.0),
                              Expanded(
                                  child: IconButton(
                                    onPressed: () {
                                      print(imgUrl.length);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => new CommentsPage(
                                                docId: docId,
                                              )));
                                    },
                                    icon: Icon(Icons.mode_comment, color: Colors.black),
                                  )),
                              Expanded(
                                  child: IconButton(
                                    onPressed: () {
                                      _launchMaps(lat, lng);
                                    },
                                    icon: Icon(Icons.location_on, color: Colors.black),
                                  )),
                              Expanded(
                                  child: IconButton(
                                    onPressed: () {
                                      //_addToSave(docId);
                                      _addToSave(docId, documentList[index]);
                                    },
                                    icon: Icon(Icons.save_alt, color: Colors.black),
                                  )),
                              Expanded(
                                  child: IconButton(
                                    onPressed: () {
                                      _onShareTap();
                                    },
                                    icon: Icon(Icons.share, color: Colors.black),
                                  )),
                              SizedBox(width: 15.0),
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                  ],
                ));
          });
    } else {
      return Center(
          child: Text(
            widget.title+" Property list is empty",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 30.0),
          ));
    }
  }


  void _launchMaps(String lat, String lng) async {
    ActionUtils.openMap(double.parse(lat), double.parse(lng));
  }

  void _addToSave(String docId, DocumentSnapshot documentList) {
    final Map<String, dynamic> map = documentList.data;
    Firestore.instance
        .collection(Constants.USERS)
        .document(Constants.USER_ID)
        .collection(Constants.SAVED)
        .document(docId)
        .setData(map)
        .then((value) => resetMsg());
  }
  resetMsg() {
    ActionUtils.showToast("Property Added into Saved");
  }


  void _launchView(String i) {
    Navigator.of(context)
        .push(
      new MaterialPageRoute(
          builder: (_) =>
          new ViewProperty(userId: Constants.USER_ID, docId: i)),
    )
        .then((val) => {getMyPropertiesDetails()});
  }
  void _onShareTap() {
    final RenderBox box = context.findRenderObject();
    Share.share(Constants.SHARE_TEXT,
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }
  getMyPropertiesDetails() {
    Firestore.instance
        .collection(Constants.PROPERTIES)
        .where("propertyStatus", isEqualTo: widget.title)
        .where("userId", isEqualTo: Constants.USER_ID)
        .getDocuments()
        .then((value) =>
        setState(() {
          documentList = value.documents;
        }));
  }
}
