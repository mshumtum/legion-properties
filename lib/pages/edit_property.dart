import 'dart:io';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/models/add_property_model.dart';

import 'ImagePreview.dart';

class EditProperty extends StatefulWidget {
  EditProperty({this.docId});

  final String docId;

  @override
  _EditPropertyState createState() => _EditPropertyState();
}

class _EditPropertyState extends State<EditProperty> {
  AddPropertyModel _propertyModel;
  String _imgURL = "", lat, lng, propStatus="Sold Property";
  String imageText = Constants.IMAGE_UPDATE_TEXT;
  File _image;
  bool _isLoading;
  double _progess = 0;
  var _phone = TextEditingController();
  var _ownerInfo = TextEditingController();
  var _houseNo = TextEditingController();
  var _houseDesc = TextEditingController();
  var _pin = TextEditingController();
  var _street = TextEditingController();
  var _address = TextEditingController();
  var _landmark = TextEditingController();
  List<NetworkImage> propertyPic = List<NetworkImage>();
  List<String> totalImage = List<String>();
  List<String> likeArray = new List<String>();
  List<dynamic> imgUrl;
  int imageIndex = 0;
  List _propStatusList;

  @override
  void initState() {
    _isLoading = false;
    propertyPic.add(NetworkImage(Constants.PROPERTY_IMAGE));
    imgUrl = new List();
    print(widget.docId);
    getData(widget.docId);
    _propStatusList = [
      "",
      "Active Property",
      "Under Construction",
      "Sold Property"
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    /*  _ownerInfo = TextEditingController(text: _propertyModel.ownerName);
    _phone = TextEditingController(text: _propertyModel.ownerContact);
    _houseNo = TextEditingController(text: _propertyModel.houseNo);
    _houseDesc = TextEditingController(text: _propertyModel.houseDescription);
    _street = TextEditingController(text: _propertyModel.streetName);
    _address = TextEditingController(text: _propertyModel.address);
    _pin = TextEditingController(text: _propertyModel.pin);
    _uploadedFileURL = _propertyModel.imgUrl;*/
    return Scaffold(
      appBar: AppBar(title: Text(Constants.EDIT_PROPERTY)),
      body: Stack(
        children: <Widget>[
          _showForm(context),
          _showCircularProgress(),
        ],
      ),
    );
  }

  Widget _showForm(BuildContext context) {
    return new Container(
      padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 12.0),
      child: new ListView(
        shrinkWrap: true,
        children: <Widget>[
          showImage(context),
          showImageIcon(),
          showOwnerName(),
          showOwnerContact(),
          showHouseNumber(),
          showStatus(),
          showHouseDiscription(),
          showStreetName(),
          showAddress(),
          showLandmark(),
          showPinCode(),
          showPrimaryButton()
        ],
      ),
    );
  }

  Widget showImage(BuildContext context) {
    if (_imgURL.length > 0 && _imgURL != null) {
      return new GestureDetector(
          child: new Container(
        child: new Image.file(_image),
        alignment: Alignment.center,
        height: 270.0,
      ));
    } else {
      return new GestureDetector(
          child: Column(
              // To centralize the children.
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
            // First child
            SizedBox(
                height: 220.0,
                width: 350.0,
                child: Carousel(
                  images: propertyPic,
                  dotSize: 4.0,
                  dotSpacing: 15.0,
                  dotColor: Colors.lightGreenAccent,
                  indicatorBgPadding: 5.0,
                  dotBgColor: Colors.brown.withOpacity(0.3),
                  borderRadius: false,
                  autoplay: false,
                  onImageChange: (prev, next) {
                    setState(() {
                      imageIndex = next;
                      //  showToast(imageIndex.toString());
                    });
                  },
                  onImageTap: (index) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ImagePreview(imgUrl[index])));
                  },
                )),
            // Second child
            FlatButton(
              child: new Text("Remove",
                  style: new TextStyle(
                      fontSize: 18.0, fontWeight: FontWeight.w300)),
              onPressed: deleteImage,
            ),
          ]));
    }
  }

  Widget showImageIcon() {
    return new Container(
      child: new Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
              child: new FlatButton(
                child: new Text(imageText,
                    style: new TextStyle(
                        fontSize: 18.0, fontWeight: FontWeight.w300)),
                onPressed: cameraMethod,
              ),
            ),
            new Icon(Icons.camera_alt,
                color: const Color(0xFF000000), size: 30.0)
          ]),
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      alignment: Alignment.centerRight,
    );
  }

  Widget showOwnerName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Owner Name',
            icon: new Icon(
              Icons.person_pin,
              color: Colors.grey,
            )),
        controller: _ownerInfo,
      ),
    );
  }

  Widget showOwnerContact() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.phone,
        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Owner Contact Number',
            icon: new Icon(
              Icons.phone,
              color: Colors.grey,
            )),
        controller: _phone,
      ),
    );
  }

  Widget showHouseNumber() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Property Name',
            icon: new Icon(
              Icons.home,
              color: Colors.grey,
            )),
        controller: _houseNo,
      ),
    );
  }
  Widget showStatus() {
    return Padding(
        padding: const EdgeInsets.fromLTRB(30.0, 30.0, 15.0, 0.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text('Property Status*',
                    style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.black))
              ],
            ),
            SizedBox(height: 10.0),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: DropdownButton<String>(
                      icon: Icon(
                        Icons.keyboard_arrow_down,
                        size: 30.0,
                        color: Colors.black,
                      ),
                      isExpanded: true,
                      items: _propStatusList?.map((item) {
                        return new DropdownMenuItem<String>(
                            child: new Text(
                              item,
                              style: TextStyle(fontSize: 14.0),
                            ),
                            value: item);
                      })?.toList() ??
                          [],
                      onChanged: (String valueSelected) async {
                        setState(() {
                          propStatus = valueSelected;
                          print(propStatus);
                        });
                      },
                      hint: Text(Constants.PORP_STATUS),
                      value: propStatus,
                    ),
                  ),
                ),
              ],
            )
          ],
        ));
  }
  Widget showHouseDiscription() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Property Description like 3BHK',
            icon: new Icon(
              Icons.home,
              color: Colors.grey,
            )),
        controller: _houseDesc,
      ),
    );
  }

  Widget showStreetName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Enter Street name',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _street,
      ),
    );
  }

  Widget showAddress() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Enter Address',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _address,
      ),
    );
  }

  Widget showLandmark() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Landmark',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _landmark,
      ),
    );
  }

  Widget showPinCode() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Postal code',
            icon: new Icon(
              Icons.local_post_office,
              color: Colors.grey,
            )),
        controller: _pin,
      ),
    );
  }

  Widget showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.brown,
            child: new Text('Update Property',
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: updateProperty,
          ),
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  cameraMethod() async {
    _image = await ImagePicker.pickImage(
        source: ImageSource.camera,
        maxHeight: 1000,
        maxWidth: 1200,
        imageQuality: 70);

    setState(() {
      _imgURL = _image.toString();
    });
    print(_image.toString());
    uploadFile(file: _image);
  }

  Future<void> uploadFile({File file}) async {
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('properties/${Path.basename(_image.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    uploadTask.events.listen((event) {
      setState(() {
        _isLoading = true;
        _progess = event.snapshot.bytesTransferred.toDouble() *
            100 /
            event.snapshot.totalByteCount.toDouble();
        print(_progess);
        imageText =
            Constants.UPLOADING + " " + _progess.round().toString() + " %";
      });
    }).onError((error) {
      _isLoading = false;
      print('$error');
      setState(() {
        imageText = Constants.IMAGE_ERR;
      });
    });
    await uploadTask.onComplete;
    _isLoading = false;
    print(Constants.FILE_UPLOADED);
    storageReference.getDownloadURL().then((fileURL) {
      print(fileURL);
      setState(() {
        _imgURL = "";
        propertyPic.add(NetworkImage(fileURL));
        totalImage.add(fileURL);
        imageText = Constants.FILE_UPLOADED;
      });
    });
  }

  void updateProperty() {
    //   print(_uploadedFileURL);
    if (validation()) {
      openSignOutDialog();

 /*     AddPropertyModel model = new AddPropertyModel(
          _phone.text.toString(),
          _ownerInfo.text.toString(),
          _houseNo.text.toString(),
          _houseDesc.text.toString(),
          _street.text.toString(),
          _address.text.toString(),
          _landmark.text.toString(),
          totalImage,
          lat,
          lng,
          _pin.text.toString(),
          Constants.USER_ID,
          widget.docId,
          Constants.USER_PIC,
          Constants.USER_NAME,
          Constants.USER_CHAT_NAME,
          propStatus,
          likeArray);
      Firestore.instance
          .collection(Constants.PROPERTIES)
          .document(widget.docId)
          .updateData(model.toJson())
          .then((value) => resetScreen());*/
    }
  }

  openSignOutDialog() async {
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new Container(
              child: Text("Are you sure to post Ads?"),
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              new FlatButton(
                  child: const Text('Post'),
                  onPressed: () {
                    Navigator.pop(context);
                    AddPropertyModel model = new AddPropertyModel(
                        _phone.text.toString(),
                        _ownerInfo.text.toString(),
                        _houseNo.text.toString(),
                        _houseDesc.text.toString(),
                        _street.text.toString(),
                        _address.text.toString(),
                        _landmark.text.toString(),
                        totalImage,
                        lat,
                        lng,
                        _pin.text.toString(),
                        Constants.USER_ID,
                        widget.docId,
                        Constants.USER_PIC,
                        Constants.USER_NAME,
                        Constants.USER_CHAT_NAME,
                        propStatus,
                        likeArray);
                    Firestore.instance
                        .collection(Constants.PROPERTIES)
                        .document(widget.docId)
                        .updateData(model.toJson())
                        .then((value) => resetScreen());
                  })
            ],
          );
        });
  }
  resetScreen() {
    Navigator.pop(context);
  }

  bool validation() {
    print(!_isLoading);
    if (_ownerInfo.text.length > 2 && _phone.text.length > 8) {
      if (_houseNo.text.length > 4) {
        if (_address.text.length > 5 && _street.text.length > 2) {
          if (_pin.text.length > 3) {
            if (totalImage.length != 0) {
              if (!_isLoading) {
                setState(() {
                  _isLoading = true;
                });
                return true;
              } else {
                showToast("Property picture is uploading");
                return false;
              }
            } else {
              showToast("Please Upload at least one property image.");
            }
          } else {
            showToast("Enter valid postal code");
            return false;
          }
        } else {
          showToast("Enter valid street name & Address");
          return false;
        }
      } else {
        showToast("Enter valid House Number or Property name");
        return false;
      }
    } else {
      showToast("Enter valid Owner Information");
      return false;
    }
  }

  void showToast(String msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
    );
  }

  void getData(String docId) {
    print(docId);
    Firestore.instance
        .collection(Constants.PROPERTIES)
        .document(docId)
        .get()
        .then((value) => {
              setState(() {
                print(value.data["pin"]);
                _phone =
                    TextEditingController(text: value.data["ownerContact"]);
                _ownerInfo =
                    TextEditingController(text: value.data["ownerName"]);
                _houseNo = TextEditingController(text: value.data["houseNo"]);
                _houseDesc =
                    TextEditingController(text: value.data["houseDescription"]);
                _street = TextEditingController(text: value.data["streetName"]);
                _address = TextEditingController(text: value.data["address"]);
                _pin = TextEditingController(text: value.data["pin"]);
                _landmark = TextEditingController(text: value.data["landmark"]);
                imgUrl = value.data["imgUrl"];
                propStatus = value.data["propertyStatus"];
                lng = value.data["lng"];
                lat = value.data["lat"];
                likeArray = value.data["likeArray"].cast<String>();
                ;

                propertyPic.clear();
                for (var i = 0; i < imgUrl.length; i++) {
                  propertyPic.add(NetworkImage(imgUrl[i]));
                  totalImage.add(imgUrl[i]);
                  //print(imgUrl[i]);
                }
              })
            });
  }

  void deleteImage() {
    if (propertyPic.length == 1) {
      showToast("Please add one more image of property");
    } else {
      setState(() {
        propertyPic.removeAt(imageIndex);
        totalImage.removeAt(imageIndex);
      });
    }
  }


}
