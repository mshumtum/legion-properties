import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/pages/StartScreen/root_page.dart';
import 'package:real_estate/services/authentication.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  TextEditingController _nameController;
  TextEditingController _emailController;
  double _progess = 0;
  TextEditingController _phoneController;
  TextEditingController _localityController;
  TextEditingController _pincodeController;
  TextEditingController _webNameController;
  File _image;
  final picker = ImagePicker();
  String _uploadedFileURL = "",
      _gender,
      _checkProfilePic = "",
      picStr = "Change profile photo";
  bool _isLoading = false, _saveLoad = false;
  List _genderList;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController(text: Constants.USER_NAME);
    _emailController = TextEditingController(text: Constants.USER_EMAIL);
    _phoneController = TextEditingController(text: Constants.USER_CONTACT);
    if (Constants.USER_GENDER == "") {
      _gender = "Select gender";
    } else {
      _gender = Constants.USER_GENDER;
    }
    _localityController = TextEditingController(text: Constants.USER_LOCALITY);
    _pincodeController = TextEditingController(text: Constants.USER_PIN);
    _webNameController = TextEditingController(text: Constants.USER_WEB);
    _uploadedFileURL = Constants.USER_PIC;
    _checkProfilePic = Constants.USER_PIC;
    _genderList = ["Select gender","Male", "Female", "Other"];
    print(_genderList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Edit Profile',
            style: TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.w500,
                color: Colors.white)),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.close,
            color: Colors.white,
            size: 30.0,
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              _saveDetails();
            },
            icon: Icon(
              Icons.check,
              color: Colors.blue,
              size: 30.0,
            ),
          ),
          SizedBox(
            width: 20.0,
          )
        ],
      ),
      body: _saveLoad
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Container(
              padding: EdgeInsets.fromLTRB(15.0, 30.0, 15.0, 30.0),
              child: Column(
                children: <Widget>[
                  _editProfilePic(),
                  SizedBox(height: 20.0),
                  _editName(),
                  SizedBox(height: 20.0),
                  _editEmail(),
                  SizedBox(height: 20.0),
                  _editPhone(),
                  SizedBox(height: 20.0),
                  _editGender(),
                  SizedBox(height: 20.0),
                  _editLocality(),
                  SizedBox(height: 20.0),
                  _editPincode(),
                  SizedBox(height: 20.0),
                  _editWebName()
                ],
              ),
            )),
    );
  }

  _editProfilePic() {
    return Center(
      child: Column(
        children: <Widget>[
          CircleAvatar(
              backgroundColor: Colors.grey,
              backgroundImage: _image == null
                  ? NetworkImage(_uploadedFileURL)
                  : FileImage(_image),
              radius: MediaQuery.of(context).size.width / 8),
          SizedBox(height: 10.0),
          InkWell(
            onTap: () {
              print('profilePic');
              if (_isLoading) {
                Center(child: CircularProgressIndicator());
                ActionUtils.showToast("Uploading in progress");
              } else {
                getImage();
              }
            },
            child: Text(picStr,
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.blueAccent)),
          )
        ],
      ),
    );
    ;
  }

  _editName() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Name',
                style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.black54))
          ],
        ),
        SizedBox(height: 10.0),
        Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                controller: _nameController,
                textAlign: TextAlign.start,
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0))),
                ),
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black54,
                  fontWeight: FontWeight.w400,
                ),
                cursorColor: Colors.blue[900],
              ),
            )
          ],
        )
      ],
    );
  }

  _editEmail() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Email',
                style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.black54))
          ],
        ),
        SizedBox(height: 10.0),
        Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                controller: _emailController,
                readOnly: true,
                textAlign: TextAlign.start,
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0))),
                ),
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black54,
                  fontWeight: FontWeight.w400,
                ),
                cursorColor: Colors.blue[900],
              ),
            )
          ],
        )
      ],
    );
  }

  _editPhone() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Phone',
                style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.black54))
          ],
        ),
        SizedBox(height: 10.0),
        Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                controller: _phoneController,
                textAlign: TextAlign.start,
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0))),
                ),
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black54,
                  fontWeight: FontWeight.w400,
                ),
                cursorColor: Colors.blue[900],
              ),
            )
          ],
        )
      ],
    );
  }

  _editGender() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Gender',
                style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.black54))
          ],
        ),
        SizedBox(height: 10.0),
        Row(
          children: <Widget>[
            Expanded(
              child: Container(
                child: DropdownButton<String>(
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      size: 30.0,
                      color: Colors.black,
                    ),
                    isExpanded: true,
                    items: _genderList?.map((item) {
                          return new DropdownMenuItem<String>(
                              child: new Text(
                                item,
                                style: TextStyle(fontSize: 14.0),
                              ),
                              value: item);
                        })?.toList() ??
                        [],
                    onChanged: (String valueSelected) async {
                      setState(() {
                        _gender = valueSelected;
                        print(_gender);
                      });
                    },
                    value: _gender),
              ),
            ),
//            Expanded(
//              child: TextField(
//                controller: _genderController,
//                textAlign: TextAlign.start,
//                decoration: InputDecoration(
//                  alignLabelWithHint: true,
//                  border: UnderlineInputBorder(
//                      borderSide: BorderSide(color: Colors.white),
//                      borderRadius:
//                          const BorderRadius.all(Radius.circular(10.0))),
//                ),
//                style: TextStyle(
//                  fontSize: 16.0,
//                  color: Colors.black54,
//                  fontWeight: FontWeight.w400,
//                ),
//                cursorColor: Colors.blue[900],
//              ),
//            )
          ],
        )
      ],
    );
  }

  _editWebName() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Website',
                style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.black54))
          ],
        ),
        SizedBox(height: 10.0),
        Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                controller: _webNameController,
                textAlign: TextAlign.start,
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0))),
                ),
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black54,
                  fontWeight: FontWeight.w400,
                ),
                cursorColor: Colors.blue[900],
              ),
            )
          ],
        )
      ],
    );
  }

  _editLocality() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Locality',
                style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.black54))
          ],
        ),
        SizedBox(height: 10.0),
        Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                controller: _localityController,
                textAlign: TextAlign.start,
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0))),
                ),
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black54,
                  fontWeight: FontWeight.w400,
                ),
                cursorColor: Colors.blue[900],
              ),
            )
          ],
        )
      ],
    );
  }

  _editPincode() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Pincode',
                style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.black54))
          ],
        ),
        SizedBox(height: 10.0),
        Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                controller: _pincodeController,
                textAlign: TextAlign.start,
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0))),
                ),
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black54,
                  fontWeight: FontWeight.w400,
                ),
                cursorColor: Colors.blue[900],
              ),
            )
          ],
        )
      ],
    );
  }

  void _saveDetails() {
    print(_gender);
    if (_nameController.text.isNotEmpty && _nameController.text.length > 2) {
      if (_emailController.text.isNotEmpty &&
          EmailValidator.validate(_emailController.text)) {
        if (_phoneController.text.isNotEmpty) {
          if (_gender != "Select gender") {
            if (_localityController.text.isNotEmpty) {
              if (_pincodeController.text.isNotEmpty) {
                if (_isLoading) {
                  ActionUtils.showToast("Uploading please wait!");
                } else {
                  print('save');
                  setState(() {
                    _saveLoad = true;
                  });
                  updateUserData();
                }
              } else {
                ActionUtils.showToast('pincode field is empty');

              }
            } else {
              ActionUtils.showToast('please add your address');
            }
          } else {
            ActionUtils.showToast('please select your gender');
          }
        } else {
          ActionUtils.showToast('phone number is  not inserted');
        }
      } else {
        ActionUtils.showToast('invalid email');
      }
    } else {
      ActionUtils.showToast('name contain atleast 3 characters');
    }
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 800,
        maxWidth: 800,
        imageQuality: 70);
    setState(() {
      _image = File(pickedFile.path);
    });
    uploadFile(file: _image);
  }

  Future<void> uploadFile({File file}) async {
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('users/${Path.basename(_image.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    uploadTask.events.listen((event) {
      setState(() {
        _isLoading = true;
        //   ActionUtils.showToast(Constants.UPLOADING);
        _progess = event.snapshot.bytesTransferred.toDouble() *
            100 /
            event.snapshot.totalByteCount.toDouble();
        print(_progess);
        picStr = Constants.UPLOADING + " " + _progess.round().toString() + " %";
      });
    }).onError((error) {
      _isLoading = false;
      ActionUtils.showToast(Constants.UPLOADING_FAILED);
      print('$error');
      setState(() {
        picStr = Constants.IMAGE_ERR;
      });
    });
    await uploadTask.onComplete;
    print(Constants.FILE_UPLOADED);
    storageReference.getDownloadURL().then((fileURL) {
      _uploadedFileURL = fileURL;
      setState(() {
        _isLoading = false;
        picStr = "Change profile photo";
        ActionUtils.showToast(Constants.FILE_UPLOADED_PROFILE);
      });
    });
  }

  void updateUserData() {
    Firestore.instance
        .collection(Constants.USERS)
        .document(Constants.USER_ID)
        .updateData({
      'profileImg': _uploadedFileURL,
      'name': _nameController.text,
      'email': _emailController.text,
      'contact': _phoneController.text,
      'locality': _localityController.text,
      'pincode': _pincodeController.text,
      'gender': _gender,
      'website': _webNameController.text,
    }).then((value) => {resetWidget()});
  }

  resetWidget() {
    if (_checkProfilePic == _uploadedFileURL) {
      print("not change");
      Constants.USER_NAME = _nameController.text;
      Constants.USER_CHAT_NAME = _nameController.text.replaceAll(" ", ".");
      Constants.USER_CONTACT = _phoneController.text;
      Constants.USER_PIC = _uploadedFileURL;
      Constants.USER_LOCALITY = _localityController.text;
      Constants.USER_PIN = _pincodeController.text;
      Constants.USER_GENDER = _gender;
      Constants.USER_WEB = _webNameController.text;
      ActionUtils.showToast("Info Change Successfully");
      _saveLoad = false;
      Navigator.pop(context);
    } else {
      print("change");
      Firestore.instance
          .collection(Constants.PROPERTIES)
          .where("userId", isEqualTo: Constants.USER_ID)
          .getDocuments()
          .then((value) => value.documents.forEach((element) {
                String docId = element.data["docId"];
                print(docId);
                Firestore.instance
                    .collection(Constants.PROPERTIES)
                    .document(docId)
                    .updateData({"profileImg": _uploadedFileURL}).then(
                        (value) => resetWidget1());
              }));
    }
  }

  resetWidget1() {
    Constants.USER_NAME = _nameController.text;
    Constants.USER_CHAT_NAME = _nameController.text.replaceAll(" ", ".");
    Constants.USER_CONTACT = _phoneController.text;
    Constants.USER_PIC = _uploadedFileURL;
    Constants.USER_LOCALITY = _localityController.text;
    Constants.USER_PIN = _pincodeController.text;
    Constants.USER_GENDER = _gender;
    Constants.USER_WEB = _webNameController.text;
    ActionUtils.showToast("Info Change Successfully");
    _saveLoad = false;
    Navigator.pop(context);
  }
}
