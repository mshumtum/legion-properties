import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/pages/SavedProperty.dart';
import 'package:real_estate/pages/add_new_prop.dart';
import 'package:real_estate/pages/home_page.dart';
import 'package:real_estate/pages/profile.dart';
import 'package:real_estate/pages/search.dart';
import 'package:real_estate/services/authentication.dart';

class BottomNavigation extends StatefulWidget {
  BottomNavigation({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);
  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> with TickerProviderStateMixin {
  TabController _tabCtrl;
  int _selectedIndex = 0;
  String _uId;
  VoidCallback _logout;

  @override
  void initState() {
    super.initState();
    _uId = widget.userId;
    Constants.USER_ID=_uId;
    _tabCtrl = TabController(length: 5, vsync: this,);
    getUserData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: customBottomNavigationBar(),
      body: bottomBarItems()
    );
  }

  customBottomNavigationBar() {
    return ConvexAppBar(
        backgroundColor: Colors.white ,
        controller: _tabCtrl,
        activeColor: Colors.brown,
        color: Colors.grey[800],
        items: [
          TabItem(icon: Icons.home,title: 'Home'),
          TabItem(icon: Icons.search,title: 'Search'),
          TabItem(icon: Icons.add,title: 'Add'),
          TabItem(icon: Icons.save_alt,title: 'Saved'),
          TabItem(icon: Icons.supervisor_account,title: 'Profile'),
        ],
        onTap: (int i) {

          setState(() {
            _selectedIndex = i;
          });
        }
    );
  }

  bottomBarItems() {
    return TabBarView(
      physics: NeverScrollableScrollPhysics(),
      controller: _tabCtrl,
      children: <Widget>[
       new HomePage(userId: _uId,
          auth: widget.auth,
          logoutCallback: widget.logoutCallback),
        Search(),
        AddNewProp(_uId),
        SavedProperty(),
        Profile(auth: widget.auth,
            logoutCallback: widget.logoutCallback)
      ],
    );
  }

  void getUserData() {
    print(_uId);
    Firestore.instance.collection(Constants.USERS).document(_uId).get().then((value) => {
      setState((){

        String userName=value.data["name"];
        Constants.USER_NAME=userName;
        Constants.USER_CHAT_NAME=userName.replaceAll(" ", ".");
        Constants.USER_EMAIL=value.data["email"];
        Constants.USER_CONTACT=value.data["contact"];
        Constants.USER_PIC=value.data["profileImg"];
        Constants.USER_LOCALITY=value.data["locality"];
        Constants.USER_PIN=value.data["pincode"];
        Constants.USER_GENDER=value.data["gender"];
        Constants.USER_WEB=value.data["website"];
        Constants.GROUP_CHAT_ENABLE=value.data["groupChat"];
      })
    });
  }
}
