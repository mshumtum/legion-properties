import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/pages/StartScreen/root_page.dart';
import 'package:real_estate/services/authentication.dart';

class SignUp extends StatefulWidget {
  SignUp({this.auth, this.loginCallback});

  final BaseAuth auth;
  final VoidCallback loginCallback;

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = new GlobalKey<FormState>();
  bool isOffline = false;
  var firestoreInstance = Firestore.instance;

  String _email = "";
  String _name = "";
  String _contact = "";
  String _password = "";
  String _confirm_password = "";

  bool _isLoading;

  @override
  void initState() {
    _isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Legion Real Estate LCC'),
        ),
        body: Stack(
          children: <Widget>[
            _showForm(context),
            _showCircularProgress(),
          ],
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget _showForm(BuildContext context) {
    return new Container(
      padding: EdgeInsets.all(16.0),
      child: new Form(
        key: _formKey,
        child: new ListView(
          shrinkWrap: true,
          children: <Widget>[
            showLogo(),
            showNameInput(),
            showEmailInput(),
            showNumberInput(),
            showPasswordInput(),
            showConfirmPassInput(),
            showPrimaryButton(),
            showSecondaryButton(),
          ],
        ),
      ),
    );
  }

  Widget showLogo() {
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 48.0,
          child: Image.asset('images/icon.png'),
        ),
      ),
    );
  }

  Widget showNameInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Full Name',
            icon: new Icon(
              Icons.person,
              color: Colors.grey,
            )),
        validator: (value) => value.length < 3 ? Constants.VALID_NAME : null,
        onSaved: (value) => _name = value.trim(),
      ),
    );
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Email',
            icon: new Icon(
              Icons.mail,
              color: Colors.grey,
            )),
        validator: (value) => !checkEmail(value) ? Constants.VALID_EMAIL : null,
        onSaved: (value) => _email = value.trim(),
      ),
    );
  }

  Widget showNumberInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.number,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Contact Number',
            icon: new Icon(
              Icons.phone,
              color: Colors.grey,
            )),
        validator: (value) => value.length < 8 ? Constants.VALID_CONTACT : null,
        onSaved: (value) => _contact = value.trim(),
      ),
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Password',
            icon: new Icon(
              Icons.lock,
              color: Colors.grey,
            )),
        validator: (value) =>
            value.length < 6 ? Constants.VALID_PASSWORD : null,
        onSaved: (value) => _password = value.trim(),
      ),
    );
  }

  Widget showConfirmPassInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Confirm Password',
            icon: new Icon(
              Icons.lock,
              color: Colors.grey,
            )),
        onSaved: (value) => _confirm_password = value.trim(),
      ),
    );
  }

  Widget showSecondaryButton() {
    return new FlatButton(
        child: new Text('Already have an account',
            style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
        onPressed: () => {Navigator.pop(context)});
  }

  Widget showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.brown,
            child: new Text('Create account',
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: ()=>{
            ActionUtils.checkInternet().then((intenet) {
            if (intenet != null && intenet) {
              validateAndSubmit();
            }
            else{
            // No-Internet Case
            loadEnable(Constants.INTERNET, false);

            }})
            },
          ),
        ));
  }

  // Perform login or signup
  void validateAndSubmit() async {
    if (validateAndSave()) {
      setState(() {
        _isLoading = true;
      });
      String userId = "";
      try {
        userId = await widget.auth.signUp(_email, _password);
        if (userId == "error") {
          print('Signed in: $userId');
          loadEnable("", false);
        } else if (userId.length > 0 && userId != "error") {
          storeData(userId);
        } else {
          loadEnable("No user found,Please Signup first", false);
        }
      } catch (e) {
        print('Error: $e');
        loadEnable(e.toString(), false);
      }
    }
  }

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      if(_password == _confirm_password){
      return true;
      }else{
        loadEnable(Constants.VALID_CONFIRM_PASSWORD, false);
      }
    }else{
    return false;
  }
  }

  void loadEnable(String msg, bool load) {
    setState(() {
      _isLoading = load;
    });
    if (msg == "") {
    } else {
      ActionUtils.showToast(msg);
    }
  }

  void goIntoHomeScreen() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => new RootPage(auth: new Auth())),
    );
  }

  void storeData(String userId) {
    firestoreInstance.collection(Constants.USERS).document(userId).setData({
      "name": _name.toString(),
      "email": _email.toString(),
      "uID": userId,
      "contact": _contact.toString(),
      "profileImg":Constants.USER_IMAGE,
      "locality":"",
      "gender":"",
      "website":"",
      "groupChat":false,

    }).then((value) {
      goIntoHomeScreen();
      widget.loginCallback();
      loadEnable("Congratulation, account create successfully!", false);
    });
  }

  checkEmail(String value) {
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(value);
    return emailValid;
  }
}
