import 'dart:async';
import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/models/add_property_model.dart';

class AddNewProp extends StatefulWidget {
  AddNewProp(this.uId);

  final String uId;

  @override
  _AddNewPropState createState() => _AddNewPropState();
}

class _AddNewPropState extends State<AddNewProp> {
  final _formKey = new GlobalKey<FormState>();
  Position _currentPosition;
  double lng1 = 0.0;
  double lat1 = 0.0;
  double _progess = 0;
  String imageText = Constants.IMAGE_TEXT;
  bool _isLoading, _propertyLoad = false;
  List<Marker> allMarkers = [];
  final _phone = TextEditingController();
  final _ownerInfo = TextEditingController();
  final _houseNo = TextEditingController();
  final _houseDesc = TextEditingController();
  final _pin = TextEditingController();
  final _street = TextEditingController();
  final _address = TextEditingController();
  final _landmark = TextEditingController();
  String _errorMessage = "";
  String _imgURL = "", propStatus = "Select Status";
  String _uploadedFileURL = "";
  String _userProfilePic = Constants.USER_IMAGE;
  File _image;
  List<File> _multipleImageURI;
  List<String> _multipleImageURL;
  List<String> _likeArray;
  Completer<GoogleMapController> _controller = Completer();
  StorageReference storageReference = FirebaseStorage.instance.ref();
  var firestoreInstance = Firestore.instance;
  List _propStatusList;

  @override
  void initState() {
    super.initState();
    _propStatusList = [
      "Select Status",
      "Active Property",
      "Under Construction",
      "Sold Property"
    ];

    print(_propStatusList);
    _errorMessage = "";
    _isLoading = false;
    _likeArray = new List();
    _multipleImageURL = new List();
    _multipleImageURI = new List();
    print(Constants.USER_CHAT_NAME);
    locationMethod();
    _ownerInfo.text = Constants.USER_NAME;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add New Property"),
        centerTitle: true,
      ),
      body: _propertyLoad
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Stack(
              children: <Widget>[
                _showForm(context),
                _showCircularProgress(),
                //  showPrimaryButton(),
              ],
            ),
    );
  }

  Widget _showForm(BuildContext context) {
    return new Container(
        padding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 12.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              showDiscription(),
              showOwnerName(),
              showOwnerContact(),
              showHouseNumber(),
              showHouseDiscription(),
              showPropertyStatus(),
              showStreetName(),
              showAddress(),
              showLandmark(),
              showImageIcon(),
              ShowImage(),
              showPinCode(),
              showMap(),
              showPrimaryButton()
            ],
          ),
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget showDiscription() {
    return new FlatButton(
      onPressed: () {},
      child: new Text('Register  Property for Customers',
          style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
    );
  }

  Widget showOwnerContact() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.phone,
//        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Owner Contact Number',
            icon: new Icon(
              Icons.phone,
              color: Colors.grey,
            )),
        controller: _phone,
      ),
    );
  }

  Widget showHouseNumber() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
//        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Property Name',
            icon: new Icon(
              Icons.home,
              color: Colors.grey,
            )),
        controller: _houseNo,
      ),
    );
  }

  Widget showHouseDiscription() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
//        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Property Description like 3BHK',
            icon: new Icon(
              Icons.home,
              color: Colors.grey,
            )),
        controller: _houseDesc,
      ),
    );
  }

  Widget showPropertyStatus() {
    return Padding(
        padding: const EdgeInsets.fromLTRB(30.0, 30.0, 15.0, 0.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text('Property Status*',
                    style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.black))
              ],
            ),
            SizedBox(height: 10.0),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: DropdownButton<String>(
                      icon: Icon(
                        Icons.keyboard_arrow_down,
                        size: 30.0,
                        color: Colors.black,
                      ),
                      isExpanded: true,
                      items: _propStatusList?.map((item) {
                            return new DropdownMenuItem<String>(
                                child: new Text(
                                  item,
                                  style: TextStyle(fontSize: 14.0),
                                ),
                                value: item);
                          })?.toList() ??
                          [],
                      onChanged: (String valueSelected) async {
                        setState(() {
                          propStatus = valueSelected;
                          print(propStatus);
                        });
                      },
                      hint: Text(Constants.PORP_STATUS),
                      value: propStatus,
                    ),
                  ),
                ),
              ],
            )
          ],
        ));
  }

  Widget showOwnerName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
//        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Owner Name',
            icon: new Icon(
              Icons.person_pin,
              color: Colors.grey,
            )),
        controller: _ownerInfo,
      ),
    );
  }

  Widget showStreetName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
//        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Enter Area name',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _street,
      ),
    );
  }

  Widget showAddress() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
//        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Enter Address',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _address,
      ),
    );
  }

  Widget showLandmark() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
//        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Landmark',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _landmark,
      ),
    );
  }

  Widget showImageIcon() {
    return new Container(
      child: new Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
              child: new FlatButton(
                child: new Text(imageText,
                    style: new TextStyle(
                        fontSize: 18.0, fontWeight: FontWeight.w300)),
                onPressed: cameraMethod,
              ),
            ),
            new Icon(Icons.camera_alt,
                color: const Color(0xFF000000), size: 30.0)
          ]),
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      alignment: Alignment.centerRight,
    );
  }

  Widget ShowImage() {
    if (_imgURL.length > 0 && _imgURL != null) {
      print("hello");
      return new Container(
        /* child: new Image.file(_image),
        padding: const EdgeInsets.all(0.0),
       */
        child: CarouselSlider(
          options: CarouselOptions(),
          items: _multipleImageURI
              .map((item) => Container(
                    child: Center(
                      child: Image.file(item, fit: BoxFit.cover),
                    ),
                  ))
              .toList(),
        ),
        alignment: Alignment.center,
        height: 300.0,
      );
    } else {
      print("hiii");
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget showPinCode() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
//        autofocus: true,
        decoration: new InputDecoration(
            labelText: 'Postal code',
            icon: new Icon(
              Icons.local_post_office,
              color: Colors.grey,
            )),
        controller: _pin,
      ),
    );
  }

  Widget showMap() {
 //   if (lat1 != 0.0) {
      return new Container(
        child: new GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: LatLng(lat1, lng1),
            zoom: 17.0,
          ),
          markers: Set.from(allMarkers), //values.toSet(),
        ),
        padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
        // alignment: Alignment.center,
        height: 300.0,
      );
    // } else {
    //   return new Container(
    //     height: 100.0,
    //   );
    // }
  }

  Widget showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.brown,
            child: new Text('Add Property',
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: AddProperty,
          ),
        ));
  }

  cameraMethod() async {
    if (_isLoading) {
      showToast(Constants.UPLOADING);
    } else {
      if (_multipleImageURI.length == 4) {
        showToast(Constants.MAX_ATTACH);
      } else {
        _image = await ImagePicker.pickImage(
            source: ImageSource.camera,
            maxHeight: 1000,
            maxWidth: 1200,
            imageQuality: 70);
        //getCameraImage(_image);
        setState(() {
          _imgURL = _image.toString();
          _multipleImageURI.add(_image);
        });
        print(_imgURL);
        uploadFile(file: _image);
      }
    }
  }

  locationMethod() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() async {
        _currentPosition = position;
        lng1 = _currentPosition.longitude;
        lat1 = _currentPosition.latitude;
        markerfunction(lng1, lat1);
        print(_currentPosition);
        final coordinates = new Coordinates(
            _currentPosition.latitude, _currentPosition.longitude);
        var addresses =
            await Geocoder.local.findAddressesFromCoordinates(coordinates);
        var first = addresses.first;
        allMarkers.add(Marker(
            markerId: MarkerId("Marker"),
            draggable: true,
            onTap: () {
              print(first.addressLine);
            },
            position: LatLng(lat1, lng1)));

        _street.text = first.locality + " " + first.subAdminArea;
        _address.text = first.addressLine;
        _pin.text = first.postalCode;
      });
      //  _street,_address,_pin
    }).catchError((e) {
      print(e);
    });
  }

  void markerfunction(double longitude, double latitude) {
    print("$longitude,$latitude");
    allMarkers.add(Marker(
        markerId: MarkerId('myMarker'),
        draggable: true,
        onTap: () {
          print('Marker Tapped');
        },
        position: LatLng(longitude, latitude)));
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  Future<void> uploadFile({File file}) async {
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('properties/${Path.basename(_image.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    uploadTask.events.listen((event) {
      setState(() {
        _isLoading = true;
        _progess = event.snapshot.bytesTransferred.toDouble() *
            100 /
            event.snapshot.totalByteCount.toDouble();
        print(_progess);
        imageText =
            Constants.UPLOADING + " " + _progess.round().toString() + " %";
      });
    }).onError((error) {
      _isLoading = false;
      print('$error');
      setState(() {
        imageText = Constants.IMAGE_ERR;
      });
    });
    await uploadTask.onComplete;
    _isLoading = false;
    print(Constants.FILE_UPLOADED);
    storageReference.getDownloadURL().then((fileURL) {
      setState(() {
        _uploadedFileURL = fileURL;
        imageText = Constants.FILE_UPLOADED;
        _multipleImageURL.add(_uploadedFileURL);
      });
    });
  }

  void AddProperty() {
    if (validation()) {
      if (_houseDesc.text.length == 0) {
        _houseDesc.text = "";
      }
      setState(() {
        _propertyLoad = true;
      });
      DocumentReference documentReference =
          Firestore.instance.collection(Constants.PROPERTIES).document();
      AddPropertyModel model = new AddPropertyModel(
          _phone.text.toString(),
          _ownerInfo.text.toString(),
          _houseNo.text.toString(),
          _houseDesc.text.toString(),
          _street.text.toString(),
          _address.text.toString(),
          _landmark.text.toString(),
          _multipleImageURL,
          lat1.toString(),
          lng1.toString(),
          _pin.text.toString(),
          widget.uId,
          documentReference.documentID,
          Constants.USER_PIC,
          Constants.USER_NAME,
          Constants.USER_CHAT_NAME,
          propStatus,
          _likeArray);
      // _database.reference().child("Properties").push().set(model.toJson());
      documentReference
          .setData(model.toJson())
          .then((value) => {resetWidget()});
    }
    print(validation());
  }

  bool validation() {
    if (_ownerInfo.text.length > 2 && _phone.text.length > 8) {
      if (_houseNo.text.length > 4) {
        if (propStatus != "Select Status") {
          if (_address.text.length > 5 && _street.text.length > 2) {
            if (_pin.text.length > 3) {
              if (_uploadedFileURL.length != 0) {
                return true;
              } else {
                showToast("Please add property picture");
                return false;
              }
            } else {
              showToast("Enter valid postal code");
              return false;
            }
          } else {
            showToast("Enter valid street name & Address");
            return false;
          }
        } else {
          showToast("Select Property Status.");
        }
      } else {
        showToast("Enter valid House Number or Property name.");
        return false;
      }
    } else {
      showToast("Enter valid Owner Information");
      return false;
    }
  }

  void showToast(String msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
    );
  }

  void resetWidget() {
    _imgURL = "";
    _phone.text = "";
    _ownerInfo.text = "";
    _houseNo.text = "";
    _pin.text = "";
    _street.text = "";
    _address.text = "";
    _landmark.text = "";
    imageText = Constants.IMAGE_TEXT;
    setState(() {
      _propertyLoad = false;
    });
    showToast("Property Added!");
  }
}
