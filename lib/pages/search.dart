import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/pages/ImagePreview.dart';
import 'package:real_estate/pages/view_property.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  TextEditingController _controller;
  List<DocumentSnapshot> documentList;

  List<DocumentSnapshot> searchSnap;
  String searchStr="";
  String textNoData="Search List is empty";

  @override
  void initState() {
    searchSnap = new List();
    documentList = new List();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Search',
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.w500,
                color: Colors.white),
          ),
          bottom: PreferredSize(
            child: Padding(
                padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Card(
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      color: Colors.grey,
                      child: new TextField(
                          textInputAction: TextInputAction.search,
                          onSubmitted: (value) {
                            print(value);
                            getData(value);
                          },
                          style: TextStyle(fontSize: 17.0, color: Colors.white),
                          onChanged: (text) {
                            setState(() {
                              print(text);
                              searchStr=text;
                            });
                            //searchStuff(text).then((value) => value.documents.forEach((element) {print(element.data["address"]);}));
                          },
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Search by area postal code',
                              hintStyle: TextStyle(
                                  fontSize: 17.0, color: Colors.white),
                              contentPadding:
                                  EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 5.0),
                              suffixIcon: InkWell(
                                  onTap: () {
                                    print(searchStr);
                                    getData(searchStr);
                                  },
                                  child: Icon(Icons.search)))),
                    )),
                  ],
                )),
            preferredSize: const Size.fromHeight(40.0),
          )),
      body: showTodoList(),
    );
  }

  Widget showTodoList() {
    if (documentList.length > 0) {
      return ListView.builder(
          shrinkWrap: true,
          reverse: true,
          itemCount: documentList.length,
          itemBuilder: (BuildContext context, int index) {
            String ownerContact = documentList[index].data["ownerContact"];
            String ownerName = documentList[index].data["ownerName"];
            String houseNo = documentList[index].data["houseNo"];
            String profileImg = documentList[index].data["profileImg"];
            List<dynamic> imgUrl = documentList[index].data["imgUrl"];
            String docId = documentList[index].data["docId"];
            String userId = documentList[index].data["userId"];
            String lat = documentList[index].data["lat"];
            String lng = documentList[index].data["lng"];
            List<NetworkImage> propertyPic = List<NetworkImage>();
            for (var i = 0; i < imgUrl.length; i++) {
              propertyPic.add(NetworkImage(imgUrl[i]));
              //print(imgUrl[i]);
            }
            return Container(
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                color: Colors.brown[300],
                elevation: 10,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      leading: Container(
                        width: 120,
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(10.0),
                            image: DecorationImage(
                                image: propertyPic[0],
                                fit: BoxFit.fill)),
                      ),
                      onTap: () {
                        print("hello");
                      },
                      title:
                      Text(ownerName, style: TextStyle(color: Colors.white)),
                      subtitle: Text(houseNo,
                          style: TextStyle(color: Colors.white)),
                    ),
                    ButtonTheme.bar(
                      child: ButtonBar(
                        children: <Widget>[
                          FlatButton(
                            child: const Text('View',
                                style: TextStyle(color: Colors.white)),
                            onPressed: () {
                              _launchView(userId,docId);
                            },
                          ),
                          FlatButton(
                            child: const Text('Call',
                                style: TextStyle(color: Colors.white)),
                            onPressed: () {
                              ActionUtils.launchCaller(ownerContact);
                            },
                          ),
                          FlatButton(
                            child: const Text('MAP',
                                style: TextStyle(color: Colors.white)),
                            onPressed: () {
                              ActionUtils.openMap(double.parse(lat), double.parse(lng));
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
//            /*    return Dismissible(_launchMaps
//              key: Key(todoId),
//
//              child: ListTile(
//                title: Text(
//                  subject,
//                  style: TextStyle(fontSize: 20.0),
//                ),
//              ),
//            );*/
          });
    }
    else {
      return Center(
          child: Text(
            textNoData,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 30.0),
          ));
    }
  }
  void _launchView(String userId,String i) {
    Navigator.of(context)
        .push(
      new MaterialPageRoute(
          builder: (_) =>
          new ViewProperty(userId:  userId,docId: i)),
    );
  }
  void getData(String text) {
    Firestore.instance
        .collection(Constants.PROPERTIES)
        .where("pin", isEqualTo: text)
        .where("userId", isEqualTo: Constants.USER_ID)
        .getDocuments()
        .then((value) => addingData(value)    );
  }

  Future<QuerySnapshot> searchStuff(String search) async {
    CollectionReference col =
        Firestore.instance.collection(Constants.PROPERTIES);
    Query query = col.where('userId', isEqualTo: Constants.USER_ID);

    if (search != "" && search.isNotEmpty) {
      query = query.where('pin', isEqualTo: search);
    }

    return await query.getDocuments();
  }

  addingData(QuerySnapshot value) {
    setState(() {
      documentList.clear();
      documentList=value.documents;
      if(documentList.length == 0){
        textNoData="No Result found";
        ActionUtils.showToast("No Result found");
      }
    });
  }
}
