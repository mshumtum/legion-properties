import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/pages/Chat/chat.dart';
import 'package:real_estate/Utiles/theme.dart';
import 'package:real_estate/services/database.dart';
import 'package:real_estate/pages/Chat/groupchat_page.dart';

class ChatRoom extends StatefulWidget {
  @override
  _ChatRoomState createState() => _ChatRoomState();
}

class _ChatRoomState extends State<ChatRoom> {
  Stream chatRooms;

  Widget chatRoomsList() {
    return StreamBuilder(
      stream: chatRooms,
      builder: (context, snapshot) {
        return snapshot.hasData
            ? ListView.builder(
                itemCount: snapshot.data.documents.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return ChatRoomsTile(
                    userName: snapshot.data.documents[index].data['chatRoomId']
                        .toString()
                        .replaceAll("_", "")
                        .replaceAll(Constants.USER_CHAT_NAME, "")
                        .replaceAll('.', " "),
                    chatRoomId:
                        snapshot.data.documents[index].data["chatRoomId"],
                  );
                })
            : Container();
      },
    );
  }

  @override
  void initState() {
    getUserInfogetChats();
    super.initState();
  }

  getUserInfogetChats() async {
    DatabaseMethods().getUserChats(Constants.USER_CHAT_NAME).then((snapshots) {
      setState(() {
        chatRooms = snapshots;
        print(
            "we got the data + ${chatRooms} this is name  ${Constants.USER_CHAT_NAME}");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "images/launcher.png",
          height: 40,
        ),
        elevation: 0.0,
        centerTitle: false,
      ),
      body: Container(
        child: new ListView(
          children: <Widget>[
            showPrimaryButton(),
            chatRoomsList()
          ],
        ) ,
      //  child: chatRoomsList(),
      ),
    );
  }

  Widget showPrimaryButton() {
    if(!Constants.GROUP_CHAT_ENABLE){
      return new Padding(
          padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
          child: SizedBox(
            height: 40.0,
            child: new RaisedButton(
              elevation: 5.0,
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              color: Colors.brown,
              child: new Text('Join Legion Chat',
                  style: new TextStyle(fontSize: 20.0, color: Colors.white)),
              onPressed: joinChat,
            ),
          ));
    }else{
      return GestureDetector(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => ChatPage(groupId: "7k4oC2MvQodhumFMlVHW", userName: Constants.USER_NAME, groupName: "Legion's Chat",)));
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
          child: ListTile(
            leading: CircleAvatar(
              radius: 30.0,
              backgroundColor: Colors.blueAccent,
              child: Text("L", textAlign: TextAlign.center, style: TextStyle(color: Colors.white)),
            ),
            title: Text("Legion Chat", style: TextStyle(fontWeight: FontWeight.bold)),
            subtitle: Text("Join the conversation as "+Constants.USER_NAME, style: TextStyle(fontSize: 13.0)),
          ),
        ),
      );
    }
  }

  void joinChat() {
    Firestore.instance
        .collection(Constants.USERS)
        .document(Constants.USER_ID)
        .updateData(  { 'groupChat': true}).then((value) => {joinGroupResponse()});
  }

  joinGroupResponse() {
    setState(() {
      Constants.GROUP_CHAT_ENABLE=true;
    });
  }
}

class ChatRoomsTile extends StatelessWidget {
  final String userName;
  final String chatRoomId;

  ChatRoomsTile({this.userName, @required this.chatRoomId});

  @override
  Widget build(BuildContext context) {
    print(this.userName);
    print(this.chatRoomId);
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Chat(
                      chatRoomId: chatRoomId,
                    )));
      },
      child: Container(
        color: Colors.black26,
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 20),
        child: Row(
          children: [
            Container(
              height: 30,
              width: 30,
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(30)),
              child: Text(userName.substring(0, 1),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w300)),
            ),
            SizedBox(
              width: 12,
            ),
            Text(userName,
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w300))
          ],
        ),
      ),
    );
  }
}
