import 'package:carousel_pro/carousel_pro.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';
import 'package:real_estate/pages/Chat/chatDialog.dart';
import 'package:real_estate/pages/comments.dart';
import 'package:real_estate/pages/view_property.dart';
import 'package:real_estate/services/authentication.dart';
import 'package:share/share.dart';

import 'edit_property.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);
  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> with WidgetsBindingObserver {
  List<DocumentSnapshot> documentList;
  final databaseReference = Firestore.instance;
  int photoIndex = 0;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  bool isLiked = true;
  List<bool> likeMainArray;
  bool isLoading=false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    documentList = new List();
    likeMainArray = new List();
    likeMainArray.clear();

    ActionUtils.checkInternet().then((intenet){
      if (intenet != null && intenet) {
        // Internet Present Case
        loadEnable("", true);
        getData();
      }else {
        // No-Internet Case
        loadEnable(Constants.INTERNET, false);
      }
    });

  }

  @override
  Future<void> didChangeAppLifeCycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Widget showTodoList() {
    if (documentList.length > 0) {
      return ListView.builder(
          shrinkWrap: true,
          itemCount: documentList.length,
          itemBuilder: (BuildContext context, int index) {
            String ownerContact = documentList[index].data["ownerContact"];
            String ownerName = documentList[index].data["ownerName"];
            String houseNo = documentList[index].data["houseNo"];
            String profileImg = documentList[index].data["profileImg"];
            List<dynamic> imgUrl = documentList[index].data["imgUrl"];
            String docId = documentList[index].data["docId"];
            String userId = documentList[index].data["userId"];
            String lat = documentList[index].data["lat"];
            String lng = documentList[index].data["lng"];
            List<NetworkImage> propertyPic = List<NetworkImage>();
            for (var i = 0; i < imgUrl.length; i++) {
              propertyPic.add(NetworkImage(imgUrl[i]));
              //print(imgUrl[i]);
            }


            return Container(
                child: Column(
              children: <Widget>[
                SizedBox(height: 10.0),
                Card(
                  color: Colors.white,
                  //  clipBehavior: Clip.antiAliasWithSaveLayer,
                  //semanticContainer: true,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                  elevation: 8.0,
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 5.0,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(5, 3, 5, 1),
                            child: CircleAvatar(
                              backgroundImage: NetworkImage(profileImg),
                              radius: 20.0,
                            ),
                          ),
                          Expanded(
                              child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[textOutput(ownerName)],
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Row(
                                children: <Widget>[textOutput(houseNo)],
                              )
                            ],
                          )),
                          button(docId, index,userId)
//                          IconButton(
//                              onPressed: () => {button(docId, index)},
//                              icon: Icon(
//                                Icons.more_vert,
//                                color: Colors.black,
//                              ))
                        ],
                      ),
                      Divider(
                        color: Colors.brown,
                      ),
                      SizedBox(
                          height: 220.0,
                          width: 350.0,
                          child: Carousel(
                            images: propertyPic,
                            dotSize: 4.0,
                            dotSpacing: 15.0,
                            dotColor: Colors.lightGreenAccent,
                            indicatorBgPadding: 5.0,
                            dotBgColor: Colors.brown.withOpacity(0.3),
                            borderRadius: true,
                            autoplay: false,
                            onImageTap: (index) {
                              _launchView(docId);
                            },
                          )),
                      Row(
                        children: <Widget>[
                          SizedBox(width: 15.0),
                          Expanded(
                              child: IconButton(
                            onPressed: () {
                              print(imgUrl.length);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => new CommentsPage(
                                            docId: docId,
                                          )));
                            },
                            icon: Icon(Icons.mode_comment, color: Colors.black),
                          )),
                          Expanded(
                              child: IconButton(
                            onPressed: () {
                              _launchMaps(lat, lng);
                            },
                            icon: Icon(Icons.location_on, color: Colors.black),
                          )),
                          Expanded(
                              child: IconButton(
                            onPressed: () {
                              //_addToSave(docId);
                              _addToSave(docId, documentList[index]);
                            },
                            icon: Icon(Icons.save_alt, color: Colors.black),
                          )),
                          Expanded(
                              child: IconButton(
                            onPressed: () {
                              _onShareTap();
                            },
                            icon: Icon(Icons.share, color: Colors.black),
                          )),
                          SizedBox(width: 15.0),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
              ],
            ));
          });
    } else {
      return Center(
        // children: <Widget>[
        //   Expanded(
        //     child: Container(
        //         child: Text("Welcome. Property list is empty",
        //           textAlign: TextAlign.center,
        //           style: TextStyle(fontSize: 30.0),)),
        //   ),   ],
        //

          child: Text(
        "Welcome. Property list is empty",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 30.0),
      ));
    }
  }

  button(String docId, int i,String userId) {
    print('button');
    print(Constants.USER_ID);
    print(userId);
    print(Constants.USER_ID  == userId);
    if(Constants.USER_ID  == userId){
      return new PopupMenuButton(
          itemBuilder: (_) => <PopupMenuItem<String>>[

            new PopupMenuItem<String>(child: const Text('Edit'), value: "1"),
            new PopupMenuItem<String>(child: const Text('Delete'), value: "2"),
            new PopupMenuItem<String>(child: const Text('Report'), value: "3"),

          ],
          onSelected: (_) {
            print(_);
            if (_ == "1") {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => new EditProperty(docId: docId)),
              ).then((val) => {getData()});
            } else if(_ == "2") {
              deleteTodo(docId, i);
            }else{
              ActionUtils.showToast("Reported");
            }
          });
    }else{

      return new PopupMenuButton(
          itemBuilder: (_) => <PopupMenuItem<String>>[
            new PopupMenuItem<String>(child: const Text('Delete'), value: "2"),
            new PopupMenuItem<String>(child: const Text('Report'), value: "3"),

          ],
          onSelected: (_) {
            print(_);
             if(_ == "2") {
              deleteTodo(docId, i);
            }else{
              ActionUtils.showToast("Reported");
            }
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(
        title: new Text('Legion Homepage'),
        actions: <Widget>[
          new FlatButton(
              child: IconButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => ChatRoom()));
            },
            icon: Icon(Icons.chat, color: Colors.white),
          )),
          /*child: new Text('Logout',
                  style: new TextStyle(fontSize: 15.0, color: Colors.white)),
              onPressed: openSignOutDialog*/
        ],
      ),
      body: RefreshIndicator(
          key: refreshKey, child: showTodoList(), onRefresh: getData),
    );
  }

  void _launchMaps(String lat, String lng) async {
    ActionUtils.openMap(double.parse(lat), double.parse(lng));
  }

  void _launchView(String i) {
    Navigator.of(context)
        .push(
          new MaterialPageRoute(
              builder: (_) =>
                  new ViewProperty(userId: widget.userId, docId: i)),
        )
        .then((val) => {getData()});
  }

//
  Future<void> getData() async {
    refreshKey.currentState?.show(atTop: true);
    await Future.delayed(Duration(seconds: 2));


    if (Constants.ADMIN_EMAILS.contains(Constants.USER_EMAIL)) {
      documentList.clear();
      databaseReference
          .collection(Constants.PROPERTIES)
          .getDocuments()
          .then((QuerySnapshot snapshot) {
        setState(() {
          documentList = snapshot.documents;
        });
      });
    } else {
      documentList.clear();
      Firestore.instance
          .collection(Constants.PROPERTIES)
          .where("userId", isEqualTo: Constants.USER_ID)
          .getDocuments()
          .then((value) => setState(() {
                documentList = value.documents;
                documentList.length == 0?loadEnable("No Ads Found", false) : loadEnable("", false);


              }));
    }
  }

  void deleteTodo(d, int i) {
    try {
      databaseReference.collection(Constants.PROPERTIES).document(d).delete();
      getData();
    } catch (e) {
      print(e.toString());
    }
  }

  Widget textOutput(String ownerInfo) {
    return Text(ownerInfo,
        style: TextStyle(
            fontSize: 13.0,
            fontWeight: FontWeight.w500,
            color: Colors.black54));
  }

/*  Future<void> _addToSave(String docId) async {

  }*/   /*     child: Text(
        "Welcome. Property list is empty",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 30.0),
      )*/

  resetMsg() {
    ActionUtils.showToast("Property Added into Saved");
  }

  stateChanged(bool isShow) {
    print('menu is ${isShow ? 'showing' : 'closed'}');
  }

  void onDismiss() {}

  void likeDislike(String docId, bool bool) {
    if (bool) {
      databaseReference
          .collection(Constants.PROPERTIES)
          .document(docId)
          .updateData({
        "likeArray": FieldValue.arrayUnion([Constants.USER_ID])
      });
    } else {
      databaseReference
          .collection(Constants.PROPERTIES)
          .document(docId)
          .updateData({
        "likeArray": FieldValue.arrayRemove([Constants.USER_ID])
      });
    }
  }

  void _addToSave(String docId, DocumentSnapshot documentList) {
    final Map<String, dynamic> map = documentList.data;
    databaseReference
        .collection(Constants.USERS)
        .document(Constants.USER_ID)
        .collection(Constants.SAVED)
        .document(docId)
        .setData(map)
        .then((value) => resetMsg());
  }

  void _onShareTap() {
    final RenderBox box = context.findRenderObject();
    Share.share(Constants.SHARE_TEXT,
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  void loadEnable(String msg, bool load) {
    setState(() {
      isLoading=load;

    });
    if (msg == "") {
    } else {
      ActionUtils.showToast(msg);
    }
  }
  Widget _showCircularProgress() {
    if (isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

}
