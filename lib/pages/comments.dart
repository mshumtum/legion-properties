import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:real_estate/Utiles/ActionUtils.dart';
import 'package:real_estate/Utiles/Constant.dart';

class CommentsPage extends StatefulWidget {
  CommentsPage({this.docId});

  String docId = "";

  @override
  _CommentsPageState createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {
  Stream<QuerySnapshot> _comment;
  final _commentValue = TextEditingController();

  @override
  void initState() {
    super.initState();

    getComments(widget.docId).then((val) {
      setState(() {
        _comment = val;
      });
    });
    //getComments(widget.docId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.chevron_left,
            size: 35.0,
            color: Colors.black,
          ),
        ),
        title: Text('Comments',
            style: TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.w500,
                color: Colors.black)),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: Column(
                children: <Widget>[_buildCommentList()],
              ),
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.grey[100],
              padding: EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    radius: 20.0,
                    backgroundImage: NetworkImage(Constants.USER_PIC),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    child: TextField(
                      controller: _commentValue,
                      decoration: InputDecoration(
                        alignLabelWithHint: true,
                        hintText: "Add a comment ",
                        border: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10.0))),
                      ),
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black54,
                        fontWeight: FontWeight.w400,
                      ),
                      cursorColor: Colors.blue[900],
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  InkWell(
                      onTap: () {
                        if (_commentValue.text.length != 0) {
                          _addComments(_commentValue.text, widget.docId);
                        } else {
                          ActionUtils.showToast("Add Comment please...");
                        }
                      },
                      child: Text(
                        'Post',
                        style: TextStyle(color: Colors.indigo, fontSize: 20.0),
                      ))
                ],
              ),
            ),
        ],
      ),
    );
  }

  Future<void> _addComments(String val, docId) async {
    Map<String, dynamic> commentsMapValue = {
      Constants.USERNAME: Constants.USER_NAME,
      Constants.COMMENT: val,
      Constants.USERPIC: Constants.USER_PIC,
      Constants.TIME: DateFormat('kk:mm:ss \n EEE d MMM').format(DateTime.now()),//.millisecondsSinceEpoch,
    };
    await Firestore.instance
        .collection(Constants.PROPERTIES)
        .document(docId)
        .collection(Constants.COMMENTS)
        .add(commentsMapValue)
        .then((value) => {resetMsg()});
  }

  getComments(String docId) async {
    print("sdsadsad " + docId);
    return Firestore.instance
        .collection(Constants.PROPERTIES)
        .document(docId)
        .collection(Constants.COMMENTS)
        .orderBy(Constants.TIME)
        .snapshots();
  }

  Widget _buildCommentList() {
    return StreamBuilder(
      stream: _comment,
      builder: (ctx, snapshot) {
        return snapshot.hasData
            ? Expanded(
                child: ListView.builder(
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                    Column(
                                      children: <Widget>[
                                        CircleAvatar(
                                          radius: 18.0,
                                          backgroundImage: NetworkImage(snapshot
                                              .data
                                              .documents[index]
                                              .data[Constants.USERPIC]),
                                        ),
                                      ],
                                    ),
                                    SizedBox(width: 10.0,),
                                    Expanded(
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              SizedBox(width: 10.0),
                                              Text(
                                                snapshot.data.documents[index]
                                                    .data[Constants.USERNAME],
                                                style: TextStyle(
                                                    fontSize: 16.0,
                                                    fontWeight: FontWeight.w500,
                                                    fontStyle: FontStyle.italic),
                                              ),
                                            ],
                                          ),
//                                          SizedBox(height: 10.0),
//                                          Row(
//                                            children: <Widget>[SizedBox(width: 10.0),
//                                              Expanded(
//                                                  child: Text(
//                                                      snapshot.data.documents[index]
//                                                          .data[Constants.COMMENT],
//                                                      style: TextStyle(
//                                                          fontSize: 14.0,
//                                                          fontWeight: FontWeight.w400))),
//                                            ],
//                                          ),
//                                          SizedBox(height: 5.0),
//                                          Divider(
//                                            color: Colors.black54,
//                                          )
                                        ],
                                      ),
                                    ),

                              ],
                            ),
                            Row(
                              children: <Widget>[SizedBox(width: MediaQuery.of(context).size.width/7),
                                Expanded(
                                  child: Text(
                                      snapshot.data.documents[index]
                                          .data[Constants.COMMENT],
                                      style: TextStyle(
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.w400)),
                                )
                              ],
                            ),
                            SizedBox(height: 10.0),
                            Row(
                              children: <Widget>[SizedBox(width: MediaQuery.of(context).size.width/7),
                                Expanded(
                                  child: Text(
                                      snapshot.data.documents[index]
                                          .data[Constants.TIME].toString(),
                                      style: TextStyle(
                                          fontSize: 12.0,color: Colors.grey,
                                          fontWeight: FontWeight.w400)),
                                )
                              ],
                            ),
                            SizedBox(height: 5.0),
                            Divider(
                              color: Colors.black54,
                            )
                          ],
                        ),
                      );
                    }),
              )
            : Container(
                height: MediaQuery.of(context).size.height / 1.5,
                child: Center(
                  child: Text(
                    'No comments yet \n Be first to add a comment',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.black54,
                        fontWeight: FontWeight.w500),
                  ),
                ));
      },
    );
  }

  resetMsg() {
    _commentValue.text = "";
    ActionUtils.showToast(Constants.COMMENT_ADDED);
  }
/*
  void getCommentData(String docId) {
    print("Document id - "+docId);
  }*/
}
