import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ImagePreview extends StatelessWidget {
  String imgUrl;

  ImagePreview(this.imgUrl);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Property Image"),
        ),
        body: Container(
            child: PhotoView(
          imageProvider: NetworkImage(imgUrl),
        )));
  }
}
