import 'package:firebase_database/firebase_database.dart';

class AddPropertyModel {
  String ownerContact;
  String ownerName;
  String key;
  String houseNo;
  String houseDescription;
  String streetName;
  String address;
  String landmark;
  List<String> imgUrl;
  String lat;
  String lng;
  String pin;
  String userId;
  String docId;
  String profileImg;
  String brokerName;
  String brokerChatName;
  String propertyStatus;
  List<String> likeArray;

  AddPropertyModel(this.ownerContact,this.ownerName, this.houseNo,this.houseDescription, this.streetName,this.address,this.landmark,
      this.imgUrl, this.lat, this.lng, this.pin, this.userId, this.docId,this.profileImg,this.brokerName,this.brokerChatName,this.propertyStatus,this.likeArray);

  AddPropertyModel.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        userId = snapshot.value["userId"],
        ownerContact = snapshot.value["ownerContact"],
        ownerName = snapshot.value["ownerName"],
        houseNo = snapshot.value["houseNo"],
        houseDescription = snapshot.value["houseDescription"],
        streetName = snapshot.value["streetName"],
        address = snapshot.value["address"],
        imgUrl = snapshot.value["imgUrl"],
        lat = snapshot.value["lat"],
        lng = snapshot.value["lng"],
        pin = snapshot.value["pin"],
        docId = snapshot.value["docId"];

  toJson() {
    return {
      "userId": userId,
      "ownerContact": ownerContact,
      "ownerName": ownerName,
      "houseNo": houseNo,
      "houseDescription": houseDescription,
      "streetName": streetName,
      "landmark": landmark,
      "address": address,
      "imgUrl": imgUrl,
      "lat": lat,
      "lng": lng,
      "pin": pin,
      "docId": docId,
      "profileImg": profileImg,
      "brokerName": brokerName,
      "brokerChatName": brokerChatName,
      "propertyStatus": propertyStatus,
      "likeArray": likeArray,
    };
  }
}
