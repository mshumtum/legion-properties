class Constants{
  /*========Firebase error handling================*/
  static const String USER_NOT_FOUND="There is no user record corresponding to this identifier. The user may have been deleted.";
  static const String USER_PASSWORD_WRONG="The password is invalid or the user does not have a password.";
  static const String NETWORK_ERROR="A network error (such as timeout, interrupted connection or unreachable host) has occurred.";
  /*=================*/
  static const String EDIT="Edit";
  static const String DELETE="Delete";
  static const String USERS="users";
  static const String PROPERTIES="properties";
  static const String ADMIN="admin";
  static const String ADMIN_LIST="adminList";
  static const String COMMENTS="comments";
  static const String SAVED="saved";
  static const String TIME="time";
  static const String COMMENT="comment";
  static const String USERNAME="userName";
  static const String USERPIC="userPhoto";
  static const String SHARE_TEXT="Let's search property  on Legion! It's a fast, simple, and secure app we can use to message and call each other for free. Get it at https://play.google.com/store/apps/details?id=com.legion.real_estate&hl=en";
  static const String MYSAVEDPROP="Property Added in Saved";


  static const String UPLOADING="Uploading";
  static const String UPLOADING_FAILED="Uploading Failed";
  static const String MAX_ATTACH="Maximum 5 attachments,User can attach";
  static const String FILE_UPLOADED="File Uploaded,Add more";
  static const String FILE_UPLOADED_PROFILE="File Uploaded Successfully";
  static const String IMAGE_TEXT="Click here to take a Snap!";
  static const String IMAGE_ERR="Sorry! Please try again.";
  static const String IMAGE_UPDATE_TEXT="Click here to update a Snap!";
  static const String PROPERTY_DETAILS="Property Details";
  static const String EDIT_PROPERTY="Edit Property";
  static const String INTERNET="Check Internet Connection";
  static const String VALID_EMAIL="Enter valid email";
  static const String VALID_PASSWORD="Please enter more then six character";
  static const String VALID_NAME="Enter valid name";
  static const String VALID_CONFIRM_PASSWORD="Enter same password";
  static const String VALID_CONTACT="Enter valid contact number";
  static const String COMMENT_ADDED="Comment Added Successfully!!";
  static const String PORP_STATUS="Select Property Status";

  static int SCREEN_RESET=0;
  static List<dynamic> ADMIN_EMAILS=[];
  static const List<String> MAIN_POP_UP= <String>[
    EDIT,DELETE
  ];
  static const String SOLD_PROPERTY="Sold Property";
  static const String ACTIVE_PROPERTY="Active Property";
  static const String UNDER_CONSTRUCTION="Under Construction";
  static const List<String> PROP_STATUS= <String>["Active Property","Under Construction","Sold Property"];

  //USER-INFO
  static String USER_ID="";
  static String USER_NAME="";
  static String USER_CHAT_NAME="";
  static String USER_PIC="";
  static String USER_EMAIL="";
  static String USER_CONTACT="";
  static String USER_LOCALITY="";
  static String USER_PIN="";
  static String USER_GENDER="";
  static String USER_WEB="";
  static bool GROUP_CHAT_ENABLE=false;

  //STATIC IMAGES
  //static String PROPERTY_IMAGE  ="https://firebasestorage.googleapis.com/v0/b/real-state-570a3.appspot.com/o/properties%2Fplot-no-image.jpg?alt=media&token=a42488d3-1cdd-4f00-902a-12e238a1c358";
  static String PROPERTY_IMAGE  ="https://firebasestorage.googleapis.com/v0/b/legion-real-estate-lcc.appspot.com/o/properties%2Fplot-no-image.jpg?alt=media&token=46d5942c-bca1-43dd-b020-3a68f31f3bc4";
  // static String USER_IMAGE  ="https://firebasestorage.googleapis.com/v0/b/real-state-570a3.appspot.com/o/users%2FPngItem_5813504.png?alt=media&token=7666ff96-9d51-4129-92ab-537be037fac2";
  static String USER_IMAGE  ="https://firebasestorage.googleapis.com/v0/b/legion-real-estate-lcc.appspot.com/o/users%2FPngItem_5813504.png?alt=media&token=4a8ff86b-22a3-4acb-9e3c-54cf574f3f3b";
  static String SOLD_IMAGE="https://firebasestorage.googleapis.com/v0/b/legion-real-estate-lcc.appspot.com/o/icons%2Fsold.png?alt=media&token=430883ca-a135-47e0-97ef-3dadc315cc50";
  static String UNDER_CONSTRUCTION_IMAGE="https://firebasestorage.googleapis.com/v0/b/legion-real-estate-lcc.appspot.com/o/icons%2Fdownload.png?alt=media&token=a0e70abb-3925-41d0-b3e1-ba28277cdfc2";
  static String ACTIVE_PROP_IMAGE="https://firebasestorage.googleapis.com/v0/b/legion-real-estate-lcc.appspot.com/o/icons%2F1-40-512.png?alt=media&token=6adbf7df-3e95-4cdc-a172-41db64691950";


}